# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function impares_consecutivos(n)
    n3 = n^3
    # esse valor "pI" (primeiro ímpar) foi um "chute" baseado em testes
    pI = (n^2) - n + 1
    soma = (n)*(n-1)
    while pI <= n3
        total = (n*pI + soma)
        if n3 == total
            return pI
        else
            pI += 2
        end
    end
    return pI
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)
    m3 = m^3
    pI = (m^2) - m + 1
    soma = (m)*(m-1)
    while pI <= m3
        total = (m*pI + soma)
        if m3 == total
            print(m, " ")
            print(m3, " ")
            for j = 1:m
                print(pI + 2*(j-1), " ")
            end
            return
        else
            pI += 2
        end
    end
    return pI
end

function mostra_n(n)
    for i = 1:n
        imprime_impares_consecutivos(i)
        println("")
    end
end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
test()

mostra_n(20)